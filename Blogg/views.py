from django.shortcuts import render
from .models import Blog

def blog_list(request):
    querysets = Blog.objects.all()
    return render(request, 'blog_list.html',{'qs': querysets})

#http://127.0.0.1:8000/blog/1
#**kwargs=1
def blog_detail(request,**kwargs):
    pk = kwargs['pk']
    blog = Blog.objects.get(pk=pk)
    return render(request,'blog_detail.html',{'blog':blog})

